### Solution

1. Clone repository locally from:

    cd  # to cloned repo

2. Create virtualenv locally and install requirements:

    pip install -r requirements.txt


3. Set up db:

    ./manage.py makemigrations
    ./manage.py migrate

4. Run your application locally:

    ./manage.py runserver

5. Check your application on `127.0.0.1:8000`




