from django.conf.urls import url

from .views import IndexView

urlpatterns = [
    # url(r'^register/$', MovieRegister.as_view(), name="register"),
    url(r'^history/$', IndexView, name="history"),
]