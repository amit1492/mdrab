from rest_framework import serializers
from .models import Movie, Genre, Votes

class GenreSerializer(serializers.ModelSerializer):
    """
    Serializer for Genre model
    """

    class Meta:
        model = Genre
        fields = '__all__'
        
class MovieSerializer(serializers.ModelSerializer):
    """
    Serializer for Movie model
    """
    genre = GenreSerializer(many=True)
    votes = serializers.SerializerMethodField()

    def get_votes(self, obj):
        votes = MovieVotes.objects.filter(movie = obj)
        up = votes.filter(vote_type = 'UP').count()
        down = votes.filter(vote_type = 'DWN').count()
        
        return {'up':up,'down':down}
    
    class Meta:
        model = Movie
        fields = ('name', 'imdb_score', 'popularity', 'director', 'genre','votes')