from django.db import models
from django.db.models import TextField
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

# Create your models here.


class Genre(models.Model):
    """
    Genre model : Table for movie Genres
    """
    name = models.CharField(max_length=500)    
    class Meta:
        verbose_name = "Genre"
        verbose_name_plural = "Genres"    
    def __str__(self):
        return self.name
class Movie(models.Model):
    """
    Movie model : model for Movies
    """
    name = models.CharField(max_length=500)
    imdb_score = models.FloatField()
    popularity = models.FloatField()
    director = models.CharField(max_length=500)
    genre = models.ManyToManyField(Genre)    
    class Meta:
        verbose_name = "Movie"
        verbose_name_plural = "Movies"    
    def __str__(self):
        return self.name

VOTE_CHOICE = (
    ("UP", "UP - Vote",),
    ("DWN", "Down - Vote",),
)
DEFAULT_VOTE_CHOICE = "UP"


class Votes(models.Model):
    user = models.ForeignKey(User, related_name="+", null=True, on_delete=models.SET_NULL)
    vote_type = models.CharField(max_length=3, choices=VOTE_CHOICE, db_index=True,default=DEFAULT_VOTE_CHOICE)
    movie = models.ForeignKey(Movie, related_name="+", null=True, on_delete=models.CASCADE)

    def validate_unique(self,*args, **kwargs):
        super(Votes,self).validate_unique(*args, **kwargs)
        query = Votes.objects.filter(user = self.user)
        if query.filter(movie = self.movie).exists():
            raise ValidationError({'user':'user have already voted for the movie'})
    class meta:
        verbose_name_plural = 'Votes'
        unique_together = ('user', 'movie')


