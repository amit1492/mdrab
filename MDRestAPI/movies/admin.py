from django.contrib import admin
from .models import Movie, Genre, Votes



# Register your models here.
class MovieAdmin(admin.ModelAdmin):
    models = Movie

class GenreAdmin(admin.ModelAdmin):
    models = Genre


class VotesAdmin(admin.ModelAdmin):
    models = Votes

admin.site.register(Movie, MovieAdmin)   
admin.site.register(Genre, GenreAdmin) 

admin.site.register(Votes,VotesAdmin)